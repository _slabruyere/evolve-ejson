"""
Copyright 2019 Zeppelin Bend Pty Ltd
This file is part of ejsonbend.

ejsonbend is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ejsonbend is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with ejsonbend.  If not, see <https://www.gnu.org/licenses/>.
"""


from typing import List, Set, Union
from zepben.model.metering import ReadingType
#from zepben.cim.iec61968.metering_pb2 import MeasurementKind
#from zepben.cim.iec61970.unit.unit_symbol_pb2 import UnitSymbol
CIM_MEAS_KIND_TO_EJSON_MEAS_TYPES = {
#    (MeasurementKind.power, UnitSymbol.W): "PLG",
#    (MeasurementKind.power, UnitSymbol.VA): "QLG",
#    (MeasurementKind.voltage, UnitSymbol.V): "VMagLG"
}

CIM_READING_TO_EJSON_MEAS_TYPES = {
    ReadingType.VOLTAGE: "VMagLG",
    ReadingType.REAL_POWER: "PLG",
    ReadingType.REACTIVE_POWER: "QLG"
}


class EjsonMeasurement(object):
    def __init__(self, meas_type: str, meter_id: Union[Set, str], phase: List[str], data: List = None, elements: Set = None,
                 quality: dict = None, time_units: str = 's'):
        self.meas_type = meas_type
        self.meter_id = meter_id
        self.phs = phase
        self.data = list() if data is None else data
        self.element = elements if elements is not None else set()
        self.quality = quality if quality is not None else {'true_phase': False }
        self.time_units = time_units

    def add_data(self, timestamp, value, data_point_quality=None):
        """
        Add data to a measurement.
        TODO: Support adding values to an existing entry (e.g: same timestamp but additional value) for 3 phase meas
        :param timestamp:
        :param value:
        :param data_point_quality:
        :return:
        """
        datum = [timestamp, [value]] if data_point_quality is None else [timestamp, [value], data_point_quality]
        self.data.append(datum)

    def add_element(self, element_id):
        self.element.add(element_id)

    def to_ejson_dict(self):
        ejson = {
                  "meter_id": self.meter_id,
                  "phs": self.phs,
                  "data": self.data,
                  "quality": self.quality,
                }

        if type(self.meter_id) == set:
            ejson["meter_id"] = list(self.meter_id)
        else:
            ejson["meter_id"] = self.meter_id

        if self.element:
            ejson["element"] = list(self.element)

        return ejson


class EjsonNodeMeasurements(object):
    def __init__(self, node_id):
        self.node_id = node_id
        self.measurement_types = {}

    def add_measurement(self, measurement: EjsonMeasurement):
        meas = self.measurement_types.get(measurement.meas_type, [])
        meas.append(measurement)
        self.measurement_types[measurement.meas_type] = meas

    def to_ejson_dict(self):
        return {self.node_id: self.measurement_types}
