"""
Copyright 2019 Zeppelin Bend Pty Ltd
This file is part of ejsonbend.

ejsonbend is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ejsonbend is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with ejsonbend.  If not, see <https://www.gnu.org/licenses/>.
"""


from typing import Iterable
from ejsonbend.ejson import EjsonConnection, EjsonElement


class EjsonInfeeder(EjsonElement):

    def __init__(self, id: str, v_setpoint: float, cons: Iterable[EjsonConnection] = None, in_service: bool = True, user_data: dict = None):
        super().__init__(id, cons, in_service, user_data)
        self.v_setpoint = v_setpoint
        self.element_type = 'Infeeder'
        self.ejson[self.element_type] = {}

    def validate(self):
        """TODO"""
        pass

    def to_ejson_dict(self):
        self.ejson[self.element_type]["v_setpoint"] = self.v_setpoint
        self.ejson[self.element_type]["in_service"] = self.in_service

        self.prepare_ejson()
        return self.ejson


