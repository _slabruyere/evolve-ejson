"""
Copyright 2019 Zeppelin Bend Pty Ltd
This file is part of ejsonbend.

ejsonbend is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ejsonbend is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with ejsonbend.  If not, see <https://www.gnu.org/licenses/>.
"""


import logging
import json
import contextlib
import grpc
import argparse
from pathlib import Path
from zepben.model import EquipmentContainer, MetricsStore, NoMeterException
from ejsonbend.ejson import get_ejson_cons, get_ejson_node_phases, EjsonConnection
from zepben.model import ConnectivityNode, PowerTransformer, EnergyConsumer, EnergySource, ACLineSegment, \
    NoEquipmentException, Breaker, MeterReading, ReadingType
from ejsonbend.ejson.ejson_infeeder import EjsonInfeeder
from ejsonbend.ejson.ejson_line import EjsonLine
from ejsonbend.ejson.ejson_load import EjsonLoad
from ejsonbend.ejson.ejson_transformer import EjsonTransformer
from ejsonbend.ejson.ejson_node import EjsonNode
from ejsonbend.ejson import EJSON_UNITS_DEFAULT, EJSON_MEAS_UNITS_DEFAULT, EJSONEncoder, EjsonXY, EjsonLatLong, PB_EJSON_PHASE_MAPPING, CIM_PHS_TO_EJSON_WIRING, CIM_PHS_SHUNT_TO_EJSON_WIRING
from ejsonbend.ejson.ejson_measurement import CIM_READING_TO_EJSON_MEAS_TYPES, EjsonMeasurement
from zepben.cim.iec61970 import VectorGroup
from zepben.model.phases import PhaseCode
from google.protobuf.empty_pb2 import Empty
from zepben.postbox.pb_pb2_grpc import NetworkDataStub, MeterReadingsStub

logger = logging.getLogger(__name__)
EJSON_FILE = str(Path.home().joinpath(Path("network.json")))
EJSON_MEASUREMENTS_FILE = str(Path.home().joinpath(Path("measurements.json")))
EJSON_PHASES = ["r", "w", "b"]
EJSON_VOLTAGE_TYPE = "ll"

def load_credential_from_file(filepath):
    with open(filepath, 'rb') as f:
        return f.read()


# TODO: Add config file for these
#CA_CERT = load_credential_from_file(os.path.join(os.path.dirname(__file__), "cacert.pem"))
#CERT = load_credential_from_file(os.path.join(os.path.dirname(__file__), "mailman.cert"))
#KEY = load_credential_from_file(os.path.join(os.path.dirname(__file__), "mailman.key"))
CA_CERT = None
CERT = None
KEY = None


@contextlib.contextmanager
def create_client_channel(addr, token, secure=False):
    if secure:
        call_credentials = grpc.access_token_call_credentials(token)
        # Channel credential will be valid for the entire channel
        channel_credentials = grpc.ssl_channel_credentials(CA_CERT, KEY, CERT)
        # Combining channel credentials and call credentials together
        composite_credentials = grpc.composite_channel_credentials(
            channel_credentials,
            call_credentials,
        )
        channel = grpc.secure_channel(addr, composite_credentials)
    else:
        channel = grpc.insecure_channel(addr)

    yield channel


def _add_readings(measurements, meas_type, readings, equip, phase, meter_id):
    """
    TODO: Split this function into two
    :param measurements:
    :param meas_type:
    :param reading: A Reading
    :param equip:
    :param phase:
    :param meter_id:
    :return:
    """
    ejson_phase = PB_EJSON_PHASE_MAPPING[phase]
    # For single phase we need to add the meters separately to the measurements
    if phase in (PhaseCode.A, PhaseCode.B, PhaseCode.C):
        for meas in measurements:
            # TODO: Could this node have more meters than phases?
            # TODO: group meters?
            if meas.meter_id == meter_id:
                for reading in readings:
                    meas.add_data(reading.timestamp.seconds, reading.value)
                meas.add_element(equip.mrid)
                break
        else:
            ejson_meas = EjsonMeasurement(meas_type, meter_id, phase=ejson_phase, elements={equip.mrid})
            for reading in readings:
                ejson_meas.add_data(reading.timestamp.seconds, reading.value)
            measurements.append(ejson_meas)
    elif phase == PhaseCode.ABC:
        # TODO: Probably doesn't work, needs testing - Three phase we need all meters as part of one "measurement"
        if measurements:
            meas = measurements[0]
            meas.meter_id.add(meter_id)
            meas.add_element(equip.mrid)
            for datum in meas.data:
                for reading in readings:
                    if datum[0] == reading.timestamp.seconds:
                        datum[1].append(reading.value)
            else:
                for reading in readings:
                    meas.add_data(reading.timestamp.seconds, [reading.value])
        else:
            ejson_meas = EjsonMeasurement(meas_type, {meter_id}, phase=ejson_phase, elements={equip.mrid})
            for reading in readings:
                ejson_meas.add_data(reading.timestamp.seconds, reading.value)
            measurements.append(ejson_meas)

    return measurements


class CIMEJSON(object):

    def __init__(self, network_path, measurements_path):
        self.network = EquipmentContainer(MetricsStore())
        self.network_path = network_path
        self.measurements_path = measurements_path
        self.measurements_ejson = None
        self.network_ejson = None
        self.metrics_nodes = {}
        self.conn_node_to_ejson_conn = {}

    def retrieve_network(self, stub: NetworkDataStub):
        """
        Builds a CIM model from a feeder.
        TODO: at the moment we require the full network before we convert to EJSON. The main bottleneck here
              is connectivity nodes, which we need all of the associated equipment for before we translate to ejson to
              ensure we get all phases for the node. Optimisations could potentially be made here because the phase should
              be the same as whatever is connected upstream... But maybe it's better to implement a breadth-first based
              send in the server to properly solve this?
        :param stub:
        :return:
        """
        stream_response = stub.getWholeNetwork(Empty())
        for eq_msg in stream_response:
            if eq_msg.HasField("es"):
                self.network.add(eq_msg.es)
            elif eq_msg.HasField("ec"):
                self.network.add(eq_msg.ec)
            elif eq_msg.HasField("pt"):
                self.network.add(eq_msg.pt)
            elif eq_msg.HasField("acls"):
                self.network.add(eq_msg.acls)
            elif eq_msg.HasField("br"):
                self.network.add(eq_msg.br)
            elif eq_msg.HasField("bv"):
                self.network.add(eq_msg.bv)
            elif eq_msg.HasField("ai"):
                self.network.add(eq_msg.ai)
            elif eq_msg.HasField("si"):
                self.network.add(eq_msg.si)
            elif eq_msg.HasField("up"):
                self.network.add(eq_msg.up)
            elif eq_msg.HasField("mt"):
                self.network.add(eq_msg.mt)

    def _get_cons(self, equip):
        try:
            ejson_cons = get_ejson_cons(equip.terminals)
        except AttributeError:
            ejson_cons = []
        # Swap out any connections that have mappings (i.e, breaker nodes have been removed)
        for con in ejson_cons:
            if con.node in self.conn_node_to_ejson_conn:
                con.closed = self.conn_node_to_ejson_conn[con.node].closed
                con.node = self.conn_node_to_ejson_conn[con.node].node
        return ejson_cons

    def translate_network(self):
        """
        Translates a CIM network into an EJSON network.

        TODO: Move the code to dedicated "to_json" functions for each type
        :return:
        """
        components = {}
        for equip in self.network.iter_connectivitynodes():
            latlong = None
            v_base = None
            xy = None
            for term in equip.terminals:
                pos_point = term.get_pos_point()
                if pos_point is not None:
                    tmp_latlong = EjsonLatLong([pos_point.x_position, pos_point.y_position])
                    if latlong is not None:
                        if latlong != tmp_latlong:
                            logger.warning(f"Node {equip.mrid} had multiple latlongs - this is a data issue. Using {latlong} from {term.equipment.mrid}")
                    else:
                        latlong = tmp_latlong

                diag_obj = term.diagram_objects()
                # Get the first point on the first diagram object. TODO: Define a more controlled access pattern to diagram objects (should probably be keyed by something meaningful)
                diag_point = next(iter(next(iter(diag_obj)).diagram_object_points))
                if diag_point is not None:
                    xy = EjsonXY([diag_point.x_position, diag_point.y_position])
                # We have a bunch of terminals in this node and we only want to set the nominal voltage
                # to one of those components in the node. Ideally nominal voltage should be the same across
                # all equipment in the nodes - but it's possible some don't have it set. To be safe we check
                # all equipment and take the highest nominal valtage
                if v_base is not None:
                    if term.get_nominal_voltage() is not None:
                        if term.get_nominal_voltage() > v_base:
                            v_base = term.get_nominal_voltage()
                elif term.get_nominal_voltage() is not None:
                    v_base = term.get_nominal_voltage()

            if xy is None:
                xy = EjsonXY([0.0, 0.0])
            if v_base is None:
                logger.error(f"v_base could not be set accurately for node {equip.mrid}. Defaulting to 0.0")
                v_base = 0.0
            if latlong is None:
                logger.error(f"latlong could not be set accurately for node {equip.mrid}. Defaulting to [0.0, 0.0]")
                latlong = EjsonLatLong([0.0, 0.0])

            components[equip.mrid] = EjsonNode(equip.mrid, xy=xy, lat_long=latlong, v_base=v_base)
            components[equip.mrid].phases = get_ejson_node_phases(equip.terminals)
        for equip in self.network.iter_lines():
            ejson_cons = self._get_cons(equip)
            i_max = equip.rated_current.value if equip.rated_current.value != 0 else None
            components[equip.mrid] = EjsonLine(equip.mrid, i_max=i_max, length=float(equip.length),
                                               z=[equip.r, equip.x], z0=[equip.r0, equip.x0], b_chg=equip.bch,
                                               cons=ejson_cons, in_service=equip.in_service)
        for equip in self.network.iter_transformers():
            ejson_cons = self._get_cons(equip)
            end_1 = equip.get_end(0)
            end_2 = equip.get_end(1)
            v_base = [end_1.rated_u, end_2.rated_u]
            z0 = None
            # These must be in order to conform to EJSON spec.
            if end_1.r is not None and end_1.x is not None:
                z = [[end_1.r, end_1.x]]
                # Lazily don't support having end_2 values only. e.g we don't support [[], [r, x]]
                if end_2.r is not None and end_2.x is not None:
                    z.append([end_2.r, end_2.x])
            else:
                logger.error(f"r and x was not set for transformer {equip.mrid}, defaulting to 0 in ejson - this is incorrect data and should be fixed")
                z = [[0, 0], [0, 0]]
            if end_1.r0 is not None and end_1.x0 is not None:
                z0 = [[end_1.r0, end_1.x0]]
                # Lazily don't support having end_2 values only. e.g we don't support [[], [r0, x0]]
                if end_2.r0 is not None and end_2.x0 is not None:
                    z0.append([end_2.r0, end_2.x0])

            range_ = sorted([end_1.ratio_tap_changer.high_step, end_1.ratio_tap_changer.low_step])
            tap = [end_1.get_tap_changer_step()]
            taps = {"range": range_, "factor": end_1.ratio_tap_changer.step_voltage_increment / 100.0}
            nom_turns_ratio = end_1.rated_u / end_2.rated_u
            components[equip.mrid] = EjsonTransformer(equip.mrid, VectorGroup.Name(equip.vector_group), v_base=v_base, z=z, z0=z0,
                                                      cons=ejson_cons, in_service=equip.in_service,
                                                      nom_turns_ratio=nom_turns_ratio, tap=tap, taps=taps)
        for equip in self.network.iter_energysources():
            ejson_cons = self._get_cons(equip)
            components[equip.mrid] = EjsonInfeeder(equip.mrid, equip.voltage_magnitude, in_service=equip.in_service,
                                                   cons=ejson_cons)
        for equip in self.network.iter_energyconsumers():
            ejson_cons = self._get_cons(equip)
            s_nom = []
            # if ECP is present we assume it only ever has a single phase
            # TODO: always use ECP when it's a multi-phase load (requires changes in cimbend)
            for phase in equip.energy_consumer_phases:
                s_nom.append([phase.pfixed, phase.qfixed])
            # No individual phases - 3 phase load so take from EnergyConsumer
            # and divide by 3.
            if not s_nom:
                s_nom = [[equip.p/3, equip.q/3]]*3
            wiring = "unknown"
            for term in equip.terminals:
                wiring = CIM_PHS_TO_EJSON_WIRING[term.phases.phase]
            components[equip.mrid] = EjsonLoad(equip.mrid, wiring=wiring, cons=ejson_cons,
                                               in_service=equip.in_service, s_nom=s_nom)

        for equip in self.network.iter_breakers():
            # We must convert any breakers into simply a closed state on the connection for the equipment that
            # is connected to this breaker (or if closed == True we have no breaker)
            final_node = equip.get_terminal(1).connectivity_node
            breaker_node = equip.get_terminal(0).connectivity_node
            final_phase = equip.get_terminal(1).phases
            for term in equip.terminals:
                # all terminals connected to this node minus itself
                for t in term.connectivity_node:
                    if t is term:
                        continue
                    if t.equipment.mrid in components:
                        # Change connection of existing equipment to final node (bypassing breaker node).
                        for con in components[t.equipment.mrid].cons:
                            if con.node == t.connectivity_node.mrid:
                                con.node = final_node.mrid
                                con.closed = not equip.is_open()
                    else:
                        # equipment doesn't exist yet, we need to save a mapping from the connectivity node
                        # we removed to the final connectivity node (node on opposite side of breaker)
                        # Breaker terminals at the moment always maintain phase
                        self.conn_node_to_ejson_conn[term.connectivity_node.mrid] = EjsonConnection(final_node.mrid,
                                                                                                    PB_EJSON_PHASE_MAPPING[final_phase.phase],
                                                                                                    not equip.is_open())
            if breaker_node.mrid in components:
                # remove the breaker node that we have bypassed from components
                del components[breaker_node.mrid]

        self.network_ejson = {
            "components": components,
            "units": EJSON_UNITS_DEFAULT,
            "voltage_type": EJSON_VOLTAGE_TYPE,
            "phases": EJSON_PHASES
        }

    def extract_equip(self, pb_mr):
        """
        Extract the equipment from the network relevant to this MeterReading
        :param pb_mr:
        :return: The first equipment and terminal encountered, plus the relevant node
        """
        meter = self.network.get_meter(pb_mr.meterMRID)
        equips = []
        for usage_point in meter.usage_points:
            equips.extend(usage_point.equipment)
        if not equips:
            raise NoEquipmentException(f"Usage point {usage_point.mrid} has no equipment")

        equip = next(iter(equips))
        if not equip.terminals:
            logger.warning(f"Equipment {equip.mrid} had no terminals. Skipping metrics")
            raise NoEquipmentException("Equipment was incomplete and had no terminals")
        if len(equip.terminals) > 1:
            # TODO: This should never happen, but i guess if it does we should probably be deterministic about
            #   which node we choose, and the easiest way is to probably say the "downstream" node.
            logger.error(f"Couldn't associate meter with a single node, picking more or less at random")
        terminal = next(iter(equip.terminals or []), None)

        if terminal is not None:
            node = terminal.connectivity_node
        else:
            logger.warning(f"Got metrics for meter {pb_mr.meterMRID} but there was no corresponding equipment {equip.mrid}")
            raise NoEquipmentException(f"Meter {pb_mr.meterMRID} had no associated equipment")

        return equip, terminal, node

    def extract_readings(self, mr_stream, reading_type):
        """
        Extract and translate all MeterReading's in a stream.
        :param reading_type: The ReadingType for the readings in this MeterReading
        :param mr_stream: The stream of MeterReadings to consume
        :return:
        """
        for pb_mr in mr_stream:  # We don't care about time buckets, we just want all meters readings
            # Retrieve the corresponding equipment for this meter from the network model
            try:
                equip, terminal, node = self.extract_equip(pb_mr)
            except NoEquipmentException:
                logger.warning(f"Got metrics for meter {pb_mr.meterMRID} but there was no corresponding equipment associated")
                continue

            # We need the phase of the terminal connected to this node
            phase = terminal.phases
            meas_types_to_readings = self.metrics_nodes.get(node.mrid, {})   # { measurement_type: []}
            meas_type = CIM_READING_TO_EJSON_MEAS_TYPES[reading_type]
            # Operating on the same meter for all readings
            measurements = _add_readings(meas_types_to_readings.get(meas_type, []), meas_type, pb_mr.readings, equip,
                                         phase.phase, pb_mr.meterMRID)

            meas_types_to_readings[meas_type] = measurements
            self.metrics_nodes[node.mrid] = meas_types_to_readings  # {node_id: {}}

    def extract_and_translate_measurements(self, stub: MeterReadingsStub):
        """
        In EJSON we have a mapping of:
        { node: { measurement_type: [ measurement1, measurement2, ... ] } }
        Each measurement is associated with a meter, and that meter is unique per node
        For CIM, we have { time_bucket: { meter: [reading, ...] }} where each reading has a type which can be
        mapped to an EJSON type, and a meter can be associated with a given node via its power_system_resource
        To build the EJSON measurements we have to get the associated node/element from the psr_id,
        go through all the meters readings, and add each reading to the corresponding EjsonMeasurement type
        for the node.
        """
        stream_response = stub.getVoltageReadings(Empty())
        self.extract_readings(stream_response, reading_type=ReadingType.VOLTAGE)

        stream_response = stub.getReactivePowerReadings(Empty())
        self.extract_readings(stream_response, reading_type=ReadingType.REACTIVE_POWER)

        stream_response = stub.getRealPowerReadings(Empty())
        self.extract_readings(stream_response, reading_type=ReadingType.REAL_POWER)

        self.measurements_ejson = {
            'measurements': self.metrics_nodes,
            'units': EJSON_MEAS_UNITS_DEFAULT
        }

    def output_ejson(self):
        with open(self.network_path, "w") as f:
            f.write(json.dumps(self.network_ejson, cls=EJSONEncoder))

        if self.measurements_ejson is not None:
            with open(self.measurements_path, "w") as f:
                f.write(json.dumps(self.measurements_ejson, cls=EJSONEncoder))

        # with open(str(Path.home().joinpath(Path("node.json"))), "w") as f:
        #     nodes = []
        #     for node in self.measurements_ejson["measurements"]:
        #         nodes.append(node)
        #     sorted_nodes = sorted(nodes)
        #     f.write("\n".join(sorted_nodes))


def run():
    parser = argparse.ArgumentParser(description="Zepben CIM2EJSON exporter")
    parser.add_argument('server', help='Host and port of grpc server', metavar="host:port", nargs="?", default="localhost:50051")
    parser.add_argument('--network', help='Path to output EJSON Network file', default=EJSON_FILE)
    parser.add_argument('--measurements', help='Path to output EJSON Measurements file', default=EJSON_MEASUREMENTS_FILE)
    parser.add_argument('--token', help='Auth0 M2M token', default="")
    args = parser.parse_args()
    cim_ejson_translator = CIMEJSON(args.network, args.measurements)
    with create_client_channel(args.server, args.token) as channel:
        network_stub = NetworkDataStub(channel)
        metrics_stub = MeterReadingsStub(channel)
        cim_ejson_translator.retrieve_network(network_stub)
        cim_ejson_translator.translate_network()
        cim_ejson_translator.extract_and_translate_measurements(metrics_stub)
        cim_ejson_translator.output_ejson()


if __name__ == "__main__":
    run()
